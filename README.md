# README #

A tool for scaling three-dimensional meshes.

# Usage

```Bash
./mesh-scaler --input=<path> --volume=<mx>,<my>,<mz> --num-fields=<count> --visualize=<0|1> --output-volume=<mx>,<my>,<mz>
```

For example, `./mesh-scaler --input=../pc-snapshot/var.dat --volume=294,294,294 --num-fields=8 --visualize=0 --output-volume=256,256,256` loads the mesh from the binary file `var.dat` of dimensions 294^3 per field, consisting of a total of 8 fields, and rescales it to volume 256^3 using trilinear interpolation.

# Dependencies

* A C compiler that supports the C11 standard

* CMake

# Cloning

```
git clone https://bitbucket.org/jpekkila/mesh-scaler.git
git submodule update --init --recursive
```

# Building

```Bash
mkdir build && cd build
cmake .. && make -j
```

# Visualization

```Bash
git submodule update --init --recursive
mkdir build && cd build
cmake -DVISUALIZATION=ON .. && make -j
./mesh-scaler --visualize=1 <other options>
```