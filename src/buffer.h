/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <stdlib.h> // size_t

#include "datatypes.h"

typedef struct {
  size_t count;
  real* data;
} Buffer;

typedef enum {
  ZERO,
  LINEAR,
  RANDOM,
  NUM_INITCONDS,
} InitcondType;

Buffer buffer_create(const size_t count);

Buffer buffer_create_from_file(const char* path);

Buffer buffer_clone(const Buffer in);

void buffer_destroy(Buffer* buffer);

void buffer_print(const Buffer buffer);

void buffer_compare(const Buffer model, const Buffer candidate);

void buffer_set(const InitcondType initcond, Buffer* buf);

void buffer_to_file(const Buffer buffer, const char* path);

void abs_error_to_file(const Buffer model, const Buffer candidate,
                       const char* path);

void ulp_error_to_file(const Buffer model, const Buffer candidate,
                       const char* path);
