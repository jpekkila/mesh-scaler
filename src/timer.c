/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "timer.h"

#include "errchk.h"

int
timer_reset(Timer* t)
{
  const int retval = clock_gettime(CLOCK_REALTIME, t);
  ERRCHK(!retval);

  return retval;
}

long
timer_diff_nsec(const Timer start)
{
  Timer end;
  timer_reset(&end);
  const long diff = (end.tv_sec - start.tv_sec) * 1000000000l +
                    (end.tv_nsec - start.tv_nsec);
  return diff;
}

void
timer_diff_print(const Timer t)
{
  printf("Time elapsed: %g ms\n", timer_diff_nsec(t) / 1e6);
}
