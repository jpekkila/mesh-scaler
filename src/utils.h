/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

/** Returns a modulo b. The result is always positive. */
int mod(const int a, const int b);

/** Returns x*/
int clamp(const int x, const int min, const int max);

/** Returns a random long double in range [0.0, 1.0] */
long double randr(void);

/** Creates a new empty file at `path`. The contents are cleared if the file
 *  exists.
 */
void file_clear(const char* path);
