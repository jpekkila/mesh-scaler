/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "test.h"

#include <stdio.h>
#include <stdlib.h>

#include "analysis.h"
#include "block.h"
#include "buffer.h"
#include "display.h"
#include "utils.h"

void
buffer_test_rw(void)
{
  printf("Testing buffers...\n");

  const char* path = "test.out";

  srand(123456);
  for (size_t i = 0; i < 10; ++i) {
    const size_t count = rand() % 4097;

    Buffer model = buffer_create(count);
    buffer_set(LINEAR, &model);
    buffer_to_file(model, path);

    Buffer candidate = buffer_create_from_file(path);
    buffer_compare(model, candidate);

    buffer_destroy(&candidate);
    buffer_destroy(&model);
    printf("OK!\n");
  }
}

void
block_test_write_simple(void)
{
  printf("Testing block writing without halo...\n");

  const char* path = "test.out";

  srand(123456);
  for (size_t i = 0; i < 10; ++i) {
    const Volume volume = (Volume){rand() % 129, rand() % 129, rand() % 129};
    const size_t count  = volume.x * volume.y * volume.z;
    Buffer model        = buffer_create(count);
    buffer_set(LINEAR, &model);

    Block block = block_create(volume);
    buffer_set(LINEAR, &block.buffer);

    const Window window = (Window){
        .offset = (int3){0, 0, 0},
        .volume = volume,
    };

    file_clear(path);
    block_to_file(block, window, path, window);

    Buffer candidate = buffer_create_from_file(path);
    buffer_compare(model, candidate);

    buffer_destroy(&candidate);
    block_destroy(&block);
    buffer_destroy(&model);
  }
  printf("OK!\n");
}

void
block_test_write_advanced(void)
{
  printf("Testing block writing with halo... ");

  const char* path = "test.out";

  srand(123456);
  for (size_t i = 0; i < 10; ++i) {
    const int3 halo_size   = (int3){rand() % 4, rand() % 4, rand() % 4};
    const Volume subvolume = (Volume){rand() % 129, rand() % 129, rand() % 129};
    const Volume volume    = (Volume){
        subvolume.x + 2 * halo_size.x,
        subvolume.y + 2 * halo_size.y,
        subvolume.z + 2 * halo_size.z,
    };
    const size_t count = volume.x * volume.y * volume.z;

    Block block = block_create(volume);
    buffer_set(LINEAR, &block.buffer);

    const Window in_window = (Window){
        .offset = halo_size,
        .volume = subvolume,
    };
    const Window out_window = (Window){
        .offset = halo_size,
        .volume = volume,
    };
    file_clear(path);
    block_to_file(block, in_window, path, out_window);

    Buffer model = buffer_create(count);
    buffer_set(LINEAR, &model);
    Buffer candidate = buffer_create_from_file(path);

    buffer_compare_window(model, candidate, volume, in_window);

    buffer_destroy(&candidate);
    buffer_destroy(&model);

    block_destroy(&block);
  }
  printf("OK!\n");
}

void
block_test_read_simple(void)
{
  printf("Testing block reading without halo...\n");
  const char* path = "test.out";

  srand(123456);
  for (size_t i = 0; i < 10; ++i) {
    const Volume volume = (Volume){rand() % 129, rand() % 129, rand() % 129};

    Block model = block_create(volume);
    buffer_set(LINEAR, &model.buffer);

    const Window window = (Window){
        .offset = (int3){0, 0, 0},
        .volume = volume,
    };

    file_clear(path);
    block_to_file(model, window, path, window);

    Block candidate = block_create_from_file(path, volume, window);

    buffer_compare(model.buffer, candidate.buffer);

    block_destroy(&candidate);
    block_destroy(&model);
  }
  printf("OK!\n");
}

void
block_test_read_advanced(void)
{
  printf("Testing block reading with halo... ");
  const char* path = "test.out";

  srand(123456);
  for (size_t i = 0; i < 10; ++i) {
    const int3 halo_size   = (int3){rand() % 4, rand() % 4, rand() % 4};
    const Volume subvolume = (Volume){rand() % 129, rand() % 129, rand() % 129};
    const Volume volume    = (Volume){
        subvolume.x + 2 * halo_size.x,
        subvolume.y + 2 * halo_size.y,
        subvolume.z + 2 * halo_size.z,
    };

    Block model = block_create(volume);
    buffer_set(LINEAR, &model.buffer);

    const Window in_window = (Window){
        .offset = halo_size,
        .volume = subvolume,
    };
    const Window out_window = (Window){
        .offset = halo_size,
        .volume = volume,
    };
    file_clear(path);
    block_to_file(model, in_window, path, out_window);

    const Window window = (Window){
        .offset = (int3){0, 0, 0},
        .volume = volume,
    };
    Block candidate = block_create_from_file(path, volume, window);

    buffer_compare_window(model.buffer, candidate.buffer, volume, in_window);

    block_destroy(&candidate);
    block_destroy(&model);
  }
  printf("OK!\n");
}

void
block_test_rw(void)
{
  printf("Testing block RW with halo and subwindows... ");

  const char* path = "test.out";

  srand(123456);
  for (size_t i = 0; i < 10; ++i) {
    const int3 nprocs = (int3){rand() % 8 + 1, rand() % 8 + 1, rand() % 8 + 1};
    const int3 halo_size = (int3){rand() % 4, rand() % 4, rand() % 4};
    const Volume nn      = (Volume){
        (rand() % 65) + 2 * halo_size.x + 1,
        (rand() % 65) + 2 * halo_size.y + 1,
        (rand() % 65) + 2 * halo_size.z + 1,
    };
    const Volume volume = (Volume){
        nprocs.x * nn.x + 2 * halo_size.x,
        nprocs.y * nn.y + 2 * halo_size.y,
        nprocs.z * nn.z + 2 * halo_size.z,
    };
    const Volume subvolume = (Volume){
        nn.x + 2 * halo_size.x,
        nn.y + 2 * halo_size.y,
        nn.z + 2 * halo_size.z,
    };

    // Strategy
    // 1. write linear to full
    // 2. read sub blocks
    // 3. clear file
    // 4. write sub block
    // 5. read full and compare subvolume

    // Write the full block
    const Window window = (Window){
        .offset = (int3){0, 0, 0},
        .volume = volume,
    };
    Block model = block_create(volume);
    buffer_set(LINEAR, &model.buffer);
    block_to_file(model, window, path, window);

    // Read sub blocks
    Block blocks[nprocs.z][nprocs.y][nprocs.x];
    for (int pz = 0; pz < nprocs.z; ++pz) {
      for (int py = 0; py < nprocs.y; ++py) {
        for (int px = 0; px < nprocs.x; ++px) {

          const int3 offset = (int3){
              px * nn.x,
              py * nn.y,
              pz * nn.z,
          };
          const Window subwindow = (Window){
              .offset = offset,
              .volume = subvolume,
          };

          blocks[pz][py][px] = block_create_from_file(path, volume, subwindow);
        }
      }
    }

    // Clear file
    file_clear(path);

    // Write sub blocks
    for (int pz = 0; pz < nprocs.z; ++pz) {
      for (int py = 0; py < nprocs.y; ++py) {
        for (int px = 0; px < nprocs.x; ++px) {

          const Window subwindow = (Window){
              .offset = halo_size,
              .volume = nn,
          };
          const int3 out_offset = (int3){
              halo_size.x + px * nn.x,
              halo_size.y + py * nn.y,
              halo_size.z + pz * nn.z,
          };
          const Window out_window = (Window){
              .offset = out_offset,
              .volume = volume,
          };

          block_to_file(blocks[pz][py][px], subwindow, path, out_window);
        }
      }
    }

    // Read the full block and compare subvolumes
    const Window subwindow = (Window){
        .offset = halo_size,
        .volume =
            (Volume){
                volume.x - 2 * halo_size.x,
                volume.y - 2 * halo_size.y,
                volume.z - 2 * halo_size.z,
            },
    };
    Block candidate = block_create_from_file(path, volume, window);
    buffer_compare_window(model.buffer, candidate.buffer, volume, subwindow);

    // Free
    for (int pz = 0; pz < nprocs.z; ++pz)
      for (int py = 0; py < nprocs.y; ++py)
        for (int px = 0; px < nprocs.x; ++px)
          block_destroy(&blocks[pz][py][px]);

    block_destroy(&candidate);
    block_destroy(&model);
  }
  printf("OK!\n");
}

void
interpolation_test_simple(void)
{
  printf("Testing 1-to-1 interpolation...\n");

  srand(123456);
  for (size_t i = 0; i < 10; ++i) {
    const Volume volume = (Volume){
        rand() % 32 + 1,
        rand() % 32 + 1,
        rand() % 32 + 1,
    };
    printf("volume (%lu, %lu, %lu)", volume.x, volume.y, volume.z);
    Block model = block_create(volume);
    buffer_set(RANDOM, &model.buffer);

    Block candidate = block_create(volume);
    block_interpolate(model, &candidate);
    buffer_compare(model.buffer, candidate.buffer);

    block_destroy(&candidate);
    block_destroy(&model);
  }
  printf("OK!\n");
}

void
interpolation_test_advanced(void)
{
  printf("Testing interpolation with scaling...\n");

  Block model = block_create((Volume){39, 39, 39});
  buffer_set(RANDOM, &model.buffer);

  Block candidate = block_create((Volume){32, 32, 32});
  block_interpolate(model, &candidate);

  printf("Comparing the buffer before interpolation (model) and after "
         "(candidate)\n");
  buffer_print_analysis("Model", model.buffer);
  buffer_print_analysis("Candidate", candidate.buffer);

  const char* model_path = "model.out";
  buffer_to_file(model.buffer, model_path);
  const char* candidate_path = "candidate.out";
  buffer_to_file(candidate.buffer, candidate_path);

  printf("Wrote model and candidate buffers to `%s` and `%s` for further "
         "analysis.\n",
         model_path, candidate_path);

  block_destroy(&candidate);
  block_destroy(&model);
  printf("OK!\n");
}

void
display_test_simple(void)
{
  display_init();

  const size_t nm      = 32;
  const Volume mvolume = (Volume){nm, nm, 1};
  Block model          = block_create(mvolume);
  buffer_set(RANDOM, &model.buffer);

  const size_t nc      = 512;
  const Volume cvolume = (Volume){nc, nc, 1};
  Block candidate      = block_create(cvolume);
  block_interpolate(model, &candidate);

  const size_t mtex = texture_create(mvolume.x, mvolume.y);
  const size_t ctex = texture_create(cvolume.x, cvolume.y);

  while (!display_refresh()) {
    const size_t mmid = (mvolume.z / 2) * mvolume.x * mvolume.y;
    texture_update(&model.buffer.data[mmid], mtex);

    const size_t cmid = (cvolume.z / 2) * cvolume.x * cvolume.y;
    texture_update(&candidate.buffer.data[cmid], ctex);
  }

  block_destroy(&candidate);
  block_destroy(&model);
  display_quit();
}

void
display_test_varfile(const char* path, const Volume volume)
{
  display_init();

  const Volume subvolume = (Volume){volume.x, volume.y, 1};

  const Window window = (Window){
      .offset = (int3){0, 0, volume.z / 2},
      .volume = subvolume,
  };
  Block model = block_create_from_file(path, volume, window);

  const Volume trilerp_volume = (Volume){4 * 1024, 4 * 1024, 1};
  Block candidate             = block_create(trilerp_volume);
  block_interpolate(model, &candidate);

  const size_t mtex = texture_create(subvolume.x, subvolume.y);
  const size_t ctex = texture_create(trilerp_volume.x, trilerp_volume.y);

  while (!display_refresh()) {
    texture_update(model.buffer.data, mtex);
    texture_update(candidate.buffer.data, ctex);
  }
  block_destroy(&model);
  display_quit();
}

void
tests_run(void)
{
  buffer_test_rw();
  block_test_write_simple();
  block_test_write_advanced();
  block_test_read_simple();
  block_test_read_advanced();
  block_test_rw();

  interpolation_test_simple();
  interpolation_test_advanced();

  interpolation_test_simple();
  interpolation_test_advanced();

  display_test_simple();
}

void
display_varfile(const char* path, const Volume volume, const size_t num_fields)
{
  display_init();

  Block mfields[num_fields];
  Block cfields[num_fields];

  const size_t slice_depth = 3;
  const Volume subvolume   = (Volume){volume.x, volume.y, slice_depth};

  const Volume trilerp_volume = (Volume){256, 256, slice_depth};
  // const Volume trilerp_volume = (Volume){1024, 1024, slice_depth};
  for (size_t i = 0; i < num_fields; ++i) {
    const Window window = (Window){
        .offset = (int3){0, 0,
                         i * volume.z / num_fields + volume.z / num_fields / 2 -
                             slice_depth / 2},
        .volume = subvolume,
    };
    mfields[i] = block_create_from_file(path, volume, window);
    cfields[i] = block_create(trilerp_volume);
    block_interpolate(mfields[i], &cfields[i]);

    printf("FIELD %lu-------\n", i);
    buffer_print_analysis("Model", mfields[i].buffer);
    buffer_print_analysis("Interpolated", cfields[i].buffer);
  }

  size_t mtex[num_fields];
  size_t ctex[num_fields];

  for (size_t i = 0; i < num_fields; ++i) {
    mtex[i] = texture_create(subvolume.x, subvolume.y);
    ctex[i] = texture_create(trilerp_volume.x, trilerp_volume.y);
  }

  for (size_t i = 0; i < num_fields; ++i) {
    texture_update(&mfields[i].buffer.data[(slice_depth / 2) * subvolume.x *
                                           subvolume.y],
                   mtex[i]);
    texture_update(&cfields[i].buffer.data[(slice_depth / 2) *
                                           trilerp_volume.x * trilerp_volume.y],
                   ctex[i]);
  }

  while (!display_refresh()) {
  }

  for (size_t i = 0; i < num_fields; ++i) {
    block_destroy(&mfields[i]);
    block_destroy(&cfields[i]);
  }

  display_quit();
}

void
display_fields(const char* fields[], const size_t num_fields,
               const Volume volume)
{
  display_init();

  Block mfields[num_fields];
  Block cfields[num_fields];

  const size_t slice_depth = 3;
  const Volume subvolume   = (Volume){volume.x, volume.y, slice_depth};

  const Volume trilerp_volume = (Volume){256, 256, slice_depth};
  // const Volume trilerp_volume = (Volume){1024, 1024, slice_depth};
  for (size_t i = 0; i < num_fields; ++i) {
    const Window window = (Window){
        .offset = (int3){0, 0,
                         i * volume.z / num_fields + volume.z / num_fields / 2 -
                             slice_depth / 2},
        .volume = subvolume,
    };
    mfields[i] = block_create_from_file(fields[i], volume, window);
    cfields[i] = block_create(trilerp_volume);
    block_interpolate(mfields[i], &cfields[i]);

    printf("FIELD %lu-------\n", i);
    buffer_print_analysis("Model", mfields[i].buffer);
    buffer_print_analysis("Interpolated", cfields[i].buffer);
  }

  size_t mtex[num_fields];
  size_t ctex[num_fields];

  for (size_t i = 0; i < num_fields; ++i) {
    mtex[i] = texture_create(subvolume.x, subvolume.y);
    ctex[i] = texture_create(trilerp_volume.x, trilerp_volume.y);
  }

  for (size_t i = 0; i < num_fields; ++i) {
    texture_update(&mfields[i].buffer.data[(slice_depth / 2) * subvolume.x *
                                           subvolume.y],
                   mtex[i]);
    texture_update(&cfields[i].buffer.data[(slice_depth / 2) *
                                           trilerp_volume.x * trilerp_volume.y],
                   ctex[i]);
  }

  while (!display_refresh()) {
  }

  for (size_t i = 0; i < num_fields; ++i) {
    block_destroy(&mfields[i]);
    block_destroy(&cfields[i]);
  }

  display_quit();
}
