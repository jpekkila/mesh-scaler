/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "analysis.h"

#include <tgmath.h> // Type-generic math

#include "errchk.h"

real
value(const real a)
{
  return a;
}

real
min(const real a, const real b)
{
  return a < b ? a : b;
}

real
max(const real a, const real b)
{
  return a > b ? a : b;
}

real
reduce(const real* data, const size_t count, const fn_unary unary_op,
       const fn_binary binary_op)
{
  ERRCHK(count > 0);

  real res = unary_op(data[0]);

  for (size_t i = 1; i < count; ++i)
    res = binary_op(res, unary_op(data[i]));

  return res;
}

void
map(const size_t count, const fn_unary fn, real* x)
{
#pragma omp parallel for
  for (size_t i = 0; i < count; ++i)
    x[i] = fn(x[i]);
}

void
map2(const size_t count, const fn_binary fn, real* x, const real* y)
{
  // Do not allow overlap (cannot parallelize otherwise)
  ERRCHK(y + sizeof(y[0]) * count <= x || x + sizeof(x[0]) * count <= y);

#pragma omp parallel for
  for (size_t i = 0; i < count; ++i)
    x[i] = fn(x[i], y[i]);
}

size_t
filter(const size_t count, const fn_bool fn, real* x)
{
  size_t num_filtered = 0;
  for (size_t i = 0; i < count; ++i)
    if (fn(x[i]))
      x[num_filtered++] = x[i];

  return num_filtered;
}

real
arange(const real* data, const size_t count)
{
  const real hi = reduce(data, count, value, max);
  const real lo = reduce(data, count, value, min);

  return hi - lo;
}

real
expected_value(const Buffer buffer)
{
  real sum = 0;
#pragma omp parallel for reduction(+ : sum)
  for (size_t i = 0; i < buffer.count; ++i)
    sum += buffer.data[i];

  return sum / buffer.count;
}

real
variance(const Buffer buffer)
{
  const real mu = expected_value(buffer);

  real sum = 0;
#pragma omp parallel for reduction(+ : sum)
  for (size_t i = 0; i < buffer.count; ++i)
    sum += (buffer.data[i] - mu) * (buffer.data[i] - mu);

  return sum / buffer.count;
}

real
standard_deviation(const Buffer buffer)
{
  return sqrt(variance(buffer));
}

real
range(const Buffer buffer)
{
  return arange(buffer.data, buffer.count);
}

real
distortion(const Buffer model, const Buffer candidate)
{
  ERRCHK(model.count == candidate.count);

  real sum = 0;
#pragma omp parallel for reduction(+ : sum)
  for (size_t i = 0; i < model.count; ++i)
    sum += (model.data[i] - candidate.data[i]) *
           (model.data[i] - candidate.data[i]);

  return sum / model.count;
}

void
buffer_print_analysis(const char* str, const Buffer buffer)
{
  printf("%s:\n", str);
  printf("Expected value: %Lg\n", (long double)expected_value(buffer));
  printf("Variance: %Lg\n", (long double)variance(buffer));
  printf("Standard deviation: %Lg\n", (long double)standard_deviation(buffer));
  printf("Range: %Lg\n", (long double)range(buffer));
  printf("Max: %Lg\n",
         (long double)reduce(buffer.data, buffer.count, value, max));
  printf("Min: %Lg\n",
         (long double)reduce(buffer.data, buffer.count, value, min));
  printf("\n");
}
