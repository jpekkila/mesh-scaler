/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "block.h"

#include <stdio.h>
#include <tgmath.h>

#include "buffer.h"
#include "errchk.h"

static inline size_t
get_idx(const int3 pos, const Volume volume)
{
  ERRCHK(pos.x >= 0);
  ERRCHK(pos.y >= 0);
  ERRCHK(pos.z >= 0);

  ERRCHK((size_t)pos.x < volume.x);
  ERRCHK((size_t)pos.y < volume.y);
  ERRCHK((size_t)pos.z < volume.z);

  const size_t idx = pos.x + pos.y * volume.x + pos.z * volume.x * volume.y;
  ERRCHK(idx < volume.x * volume.y * volume.z);

  return idx;
}

Block
block_create(const Volume volume)
{
  const size_t count = volume.x * volume.y * volume.z;
  Block block        = (Block){
             .volume = volume,
             .buffer = buffer_create(count),
  };

  return block;
}

void
block_destroy(Block* block)
{
  block->volume = (Volume){0, 0, 0};
  buffer_destroy(&block->buffer);
}

void
block_print(const Block block)
{
  for (size_t k = 0; k < block.volume.z; ++k) {
    printf("-----%lu-------\n", k);
    for (size_t j = 0; j < block.volume.y; ++j) {
      for (size_t i = 0; i < block.volume.x; ++i) {

        const int3 pos = (int3){i, j, k};
        printf("%Lg, ",
               (long double)block.buffer.data[get_idx(pos, block.volume)]);
      }
      printf("\n");
    }
    printf("-------------\n");
  }
}

typedef struct {
  real x, y, z;
} real3;

void
block_interpolate(const Block in, Block* out)
{
#pragma omp parallel for
  for (size_t k = 0; k < out->volume.z; ++k) {
    for (size_t j = 0; j < out->volume.y; ++j) {
      for (size_t i = 0; i < out->volume.x; ++i) {

        const size_t nx = out->volume.x > 1 ? out->volume.x - 1 : 1;
        const size_t ny = out->volume.y > 1 ? out->volume.y - 1 : 1;
        const size_t nz = out->volume.z > 1 ? out->volume.z - 1 : 1;

        // Normalize output coordinates
        const real3 normalized_coordinates = (real3){
            (real)i / nx,
            (real)j / ny,
            (real)k / nz,
        }; // Range [0, 1]

        // Map to input coordinates
        const real3 pos = (real3){
            normalized_coordinates.x * (in.volume.x - 1),
            normalized_coordinates.y * (in.volume.y - 1),
            normalized_coordinates.z * (in.volume.z - 1),
        }; // Range [0, in_volume - 1]

        ERRCHK(!isnan(pos.x));
        ERRCHK(!isnan(pos.y));
        ERRCHK(!isnan(pos.z));
        ERRCHK(floor(pos.x) >= 0);
        ERRCHK(floor(pos.y) >= 0);
        ERRCHK(floor(pos.z) >= 0);
        ERRCHK(ceil(pos.x) < in.volume.x);
        ERRCHK(ceil(pos.y) < in.volume.y);
        ERRCHK(ceil(pos.z) < in.volume.z);

        // Compute lattice spacing
        real3 ds = (real3){
            (pos.x - floor(pos.x)) / (ceil(pos.x) - floor(pos.x)),
            (pos.y - floor(pos.y)) / (ceil(pos.y) - floor(pos.y)),
            (pos.z - floor(pos.z)) / (ceil(pos.z) - floor(pos.z)),
        };
        ds.x = ceil(pos.x) == floor(pos.x) ? 0 : ds.x;
        ds.y = ceil(pos.y) == floor(pos.y) ? 0 : ds.y;
        ds.z = ceil(pos.z) == floor(pos.z) ? 0 : ds.z;
        ERRCHK(!isnan(ds.x) && !isinf(ds.x));
        ERRCHK(!isnan(ds.y) && !isinf(ds.y));
        ERRCHK(!isnan(ds.z) && !isinf(ds.z));

        const int3 p000 = (int3){
            (int)floor(pos.x),
            (int)floor(pos.y),
            (int)floor(pos.z),
        };
        const int3 p001 = (int3){
            (int)floor(pos.x),
            (int)floor(pos.y),
            (int)ceil(pos.z),
        };
        const int3 p010 = (int3){
            (int)floor(pos.x),
            (int)ceil(pos.y),
            (int)floor(pos.z),
        };
        const int3 p011 = (int3){
            (int)floor(pos.x),
            (int)ceil(pos.y),
            (int)ceil(pos.z),
        };
        const int3 p100 = (int3){
            (int)ceil(pos.x),
            (int)floor(pos.y),
            (int)floor(pos.z),
        };
        const int3 p101 = (int3){
            (int)ceil(pos.x),
            (int)floor(pos.y),
            (int)ceil(pos.z),
        };
        const int3 p110 = (int3){
            (int)ceil(pos.x),
            (int)ceil(pos.y),
            (int)floor(pos.z),
        };
        const int3 p111 = (int3){
            (int)ceil(pos.x),
            (int)ceil(pos.y),
            (int)ceil(pos.z),
        };

        const real c000 = in.buffer.data[get_idx(p000, in.volume)];
        const real c001 = in.buffer.data[get_idx(p001, in.volume)];
        const real c010 = in.buffer.data[get_idx(p010, in.volume)];
        const real c011 = in.buffer.data[get_idx(p011, in.volume)];
        const real c100 = in.buffer.data[get_idx(p100, in.volume)];
        const real c101 = in.buffer.data[get_idx(p101, in.volume)];
        const real c110 = in.buffer.data[get_idx(p110, in.volume)];
        const real c111 = in.buffer.data[get_idx(p111, in.volume)];

        const real c00 = c000 * ((real)1.0 - ds.x) + c100 * ds.x;
        const real c01 = c001 * ((real)1.0 - ds.x) + c101 * ds.x;
        const real c10 = c010 * ((real)1.0 - ds.x) + c110 * ds.x;
        const real c11 = c011 * ((real)1.0 - ds.x) + c111 * ds.x;

        const real c0 = c00 * ((real)1.0 - ds.y) + c10 * ds.y;
        const real c1 = c01 * ((real)1.0 - ds.y) + c11 * ds.y;

        const real c = c0 * ((real)1.0 - ds.z) + c1 * ds.z;

        out->buffer.data[get_idx((int3){i, j, k}, out->volume)] = c;
      }
    }
  }
}

static inline int3
add(const int3 a, const int3 b)
{
  return (int3){
      a.x + b.x,
      a.y + b.y,
      a.z + b.z,
  };
}

void
block_to_file(const Block in, const Window in_window, const char* path,
              const Window out_window)
{
  ERRCHK(in_window.offset.x + in_window.volume.x <= in.volume.x);
  ERRCHK(in_window.offset.y + in_window.volume.y <= in.volume.y);
  ERRCHK(in_window.offset.z + in_window.volume.z <= in.volume.z);

  FILE* fp = fopen(path, "r+");
  ERRCHK(fp);

  for (size_t k = 0; k < in_window.volume.z; ++k) {
    for (size_t j = 0; j < in_window.volume.y; ++j) {

      const int3 pos = (int3){0, j, k};

      const int3 in_pos   = add(in_window.offset, pos);
      const size_t in_idx = get_idx(in_pos, in.volume);

      const int3 out_pos   = add(out_window.offset, pos);
      const size_t out_idx = get_idx(out_pos, out_window.volume);

      const size_t line = in_window.volume.x;

      ERRCHK(!fseek(fp, sizeof(in.buffer.data[0]) * out_idx, SEEK_SET));

      const size_t res = fwrite(&in.buffer.data[in_idx],
                                sizeof(in.buffer.data[0]), line, fp);
      ERRCHK(res == line);
    }
  }

  // Ensure the file is sufficiently large
  const size_t bytes = sizeof(in.buffer.data[0]) * out_window.volume.x *
                       out_window.volume.y * out_window.volume.z;
  ERRCHK(!fseek(fp, bytes, SEEK_SET));
  fwrite("\0", 1, 1, fp);

  fclose(fp);
}

Block
block_create_from_file(const char* path, const Volume volume,
                       const Window window)
{
  ERRCHK(window.offset.x + window.volume.x <= volume.x);
  ERRCHK(window.offset.y + window.volume.y <= volume.y);
  ERRCHK(window.offset.z + window.volume.z <= volume.z);

  FILE* fp = fopen(path, "r+");
  ERRCHK(fp);

  Block block = block_create(window.volume);

  for (size_t k = 0; k < window.volume.z; ++k) {
    for (size_t j = 0; j < window.volume.y; ++j) {

      const int3 pos = (int3){0, j, k};

      const int3 in_pos   = add(window.offset, pos);
      const size_t in_idx = get_idx(in_pos, volume);

      const size_t out_idx = get_idx(pos, window.volume);

      const size_t line = window.volume.x;
      ERRCHK(!fseek(fp, sizeof(block.buffer.data[0]) * in_idx, SEEK_SET));

      const size_t res = fread(&block.buffer.data[out_idx],
                               sizeof(block.buffer.data[0]), line, fp);
      ERRCHK(res == line);
    }
  }

  fclose(fp);
  return block;
}

void
buffer_compare_window(const Buffer model, const Buffer candidate,
                      const Volume volume, const Window window)
{
  for (size_t k = 0; k < window.volume.z; ++k) {
    for (size_t j = 0; j < window.volume.y; ++j) {
      for (size_t i = 0; i < window.volume.x; ++i) {
        const int3 pos   = add(window.offset, (int3){i, j, k});
        const size_t idx = get_idx(pos, volume);

        ERRCHK(model.data[idx] == candidate.data[idx]);
      }
    }
  }
}

void
block_set(const int3 halo, Block* block)
{
  ERRCHK(halo.x >= 0);
  ERRCHK(halo.y >= 0);
  ERRCHK(halo.z >= 0);

  for (size_t k = 0; k < block->volume.z; ++k) {
    for (size_t j = 0; j < block->volume.y; ++j) {
      for (size_t i = 0; i < block->volume.x; ++i) {
        const size_t idx = get_idx((int3){i, j, k}, block->volume);

        real value = 0;
        if (i < (size_t)halo.x || j < (size_t)halo.y || k < (size_t)halo.z ||
            i >= block->volume.x - halo.x || j >= block->volume.y - halo.y ||
            k >= block->volume.z - halo.z)
          value = 1;

        block->buffer.data[idx] = value;
      }
    }
  }
}
