/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <errno.h>  // errno
#include <stdio.h>  // fprintf
#include <stdlib.h> // exit
#include <string.h> // strerror
#include <time.h>   // ctime

#define ERROR(str)                                                             \
  {                                                                            \
    time_t terr;                                                               \
    time(&terr);                                                               \
    fprintf(stderr, "%s", ctime(&terr));                                       \
    fprintf(stderr, "\tError in file %s line %d: %s\n", __FILE__, __LINE__,    \
            str);                                                              \
    fprintf(stderr, "\tErrno: `%s`\n", errno ? strerror(errno) : "");          \
    fflush(stderr);                                                            \
    exit(EXIT_FAILURE);                                                        \
    abort();                                                                   \
  }

#define WARNING(str)                                                           \
  {                                                                            \
    time_t terr;                                                               \
    time(&terr);                                                               \
    fprintf(stderr, "%s", ctime(&terr));                                       \
    fprintf(stderr, "\tWarning in file %s line %d: %s\n", __FILE__, __LINE__,  \
            str);                                                              \
    fflush(stderr);                                                            \
  }

#define ERRCHK(retval)                                                         \
  {                                                                            \
    if (!(retval))                                                             \
      ERROR(#retval " was false");                                             \
  }
#define WARNCHK(retval)                                                        \
  {                                                                            \
    if (!(retval))                                                             \
      WARNING(#retval " was false");                                           \
  }
