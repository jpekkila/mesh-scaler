/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "utils.h"

#include <limits.h> // CHAR_BIT
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "errchk.h"

typedef unsigned int uint128_t __attribute__((mode(TI)));

int
mod(const int a, const int b)
{
  const int r = a % b;
  return r < 0 ? r + b : r;
}

int
clamp(const int x, const int min, const int max)
{
  return x > max ? max : x < min ? min : x;
}

/** Returns a random long double in range [0.0, 1.0] */
long double
randr(void)
{
#define GEN_RAND_BIN() (rand() >= RAND_MAX / 2)

  uint128_t x = GEN_RAND_BIN();
  for (size_t i = 0; i < CHAR_BIT * sizeof(x); ++i)
    x = (x << 1) | GEN_RAND_BIN();

  const long double retval = (long double)x / (long double)(uint128_t)(-1);

  ERRCHK(retval >= 0);
  ERRCHK(retval <= 1);
  return retval;
}

void
file_clear(const char* path)
{
  FILE* fp = fopen(path, "w");
  ERRCHK(path);
  fclose(fp);
}
