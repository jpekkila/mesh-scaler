/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "display.h"

#include <math.h> // ceil

#include "analysis.h" // arange, reduce
#include "errchk.h"

#if VISUALIZATION
#include "SDL.h"

#define DISPLAY_WIDTH (2400)
#define DISPLAY_HEIGHT (1200)
#define FULLSCREEN (0)

static SDL_Renderer* renderer = NULL;
static SDL_Window* window     = NULL;

#define MAX_NUM_TEXTURES (32)
static SDL_Texture* textures[MAX_NUM_TEXTURES];
static size_t num_textures = 0;

#define ERRCHK_SDL(retval)                                                     \
  {                                                                            \
    if (!(retval)) {                                                           \
      fprintf(stderr, "SDL error %s\n", SDL_GetError());                       \
      ERROR(#retval " was false");                                             \
    }                                                                          \
  }

/*
 * =============================================================================
 * Camera
 * =============================================================================
 */
typedef struct {
  float x, y;
} float2;

typedef struct {
  float x, y, w, h;
} float4;

typedef struct {
  float2 pos;
  float scale;
} Camera;

static Camera camera = {0};

/** Performs orthographic projection on a rectangle.
 *  `pos` is the center point of the rectangle
 *  `dims` are the dimensions of the rectangle
 */
static inline float4
project_ortho(const float2 pos, const float2 dims, const float2 wdims)
{
  const float2 radius = (float2){0.5f * dims.x, 0.5f * dims.y};

  const float4 rect = (float4){
      camera.scale * (pos.x - camera.pos.x) + 0.5f * wdims.x,
      camera.scale * (pos.y - camera.pos.y) + 0.5f * wdims.y,
      camera.scale * radius.x,
      camera.scale * radius.y,
  };
  return rect;
}

static inline SDL_Rect
project_ortho_to_rect(const SDL_Rect in)
{
  const float2 pos  = (float2){in.x, in.y};
  const float2 dims = (float2){in.w, in.h};

  int w, h;
  SDL_GetWindowSize(window, &w, &h);
  const float2 wdims = (float2){w, h};

  const float4 rectf = project_ortho(pos, dims, wdims);
  const SDL_Rect out = (SDL_Rect){
      (int)(rectf.x - rectf.w),
      (int)(h - rectf.y - rectf.h),
      (int)ceilf(2.0f * rectf.w),
      (int)ceilf(2.0f * rectf.h),
  };

  return out;
}

/*
 * =============================================================================
 * Input
 * =============================================================================
 */
static int
eval_input(void)
{
  SDL_Event e;
  while (SDL_PollEvent(&e)) {
    if (e.type == SDL_QUIT) {
      return -1;
    }
    else if (e.type == SDL_KEYDOWN) {
      if (e.key.keysym.sym == SDLK_ESCAPE)
        return -1;
    }
  }
  return 0;
}

bool
input_keydown(const Key code)
{
  ERRCHK(code < NUM_KEYS);

  const uint8_t* keystates          = (uint8_t*)SDL_GetKeyboardState(NULL);
  const SDL_Scancode keys[NUM_KEYS] = {
      [ZOOM_IN] = SDL_SCANCODE_KP_PLUS, [ZOOM_OUT] = SDL_SCANCODE_KP_MINUS,
      [MV_UP] = SDL_SCANCODE_UP,        [MV_DOWN] = SDL_SCANCODE_DOWN,
      [MV_LEFT] = SDL_SCANCODE_LEFT,    [MV_RIGHT] = SDL_SCANCODE_RIGHT,

  };
  return keystates[keys[code]];
}

/*
 * =============================================================================
 * Display
 * =============================================================================
 */
void
display_init(void)
{
  ERRCHK_SDL(!SDL_Init(SDL_INIT_EVERYTHING));

  // window
  window = SDL_CreateWindow("mesh-scaler", SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, DISPLAY_WIDTH,
                            DISPLAY_HEIGHT, SDL_WINDOW_SHOWN);
  ERRCHK_SDL(window);

  if (FULLSCREEN)
    ERRCHK_SDL(SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP));

  // Renderer
  const uint32_t flags = 0; // SDL_RENDERER_ACCELERATED;
  renderer             = SDL_CreateRenderer(window, -1, flags);
  ERRCHK_SDL(renderer);

  // Renderer options
  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"); // Linear filtering

  // Camera
  camera.pos   = (float2){0, 0};
  camera.scale = 1;
}

void
display_quit(void)
{
  for (size_t i = 0; i < num_textures; ++i)
    SDL_DestroyTexture(textures[i]);
  num_textures = 0;

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

int
display_refresh(void)
{
  const int target_ms  = 1000 / 60;
  static int start_ms  = 0;
  const int ms_elapsed = SDL_GetTicks() - start_ms;
  if (ms_elapsed < target_ms)
    SDL_Delay(target_ms - ms_elapsed);

  const int frametime_ms = SDL_GetTicks() - start_ms;
  start_ms               = SDL_GetTicks();

  // Clear
  SDL_SetRenderDrawColor(renderer, 56, 60, 74, 1);
  SDL_RenderClear(renderer);

  // Draw
  int w, h;
  ERRCHK(!SDL_QueryTexture(textures[0], NULL, NULL, &w, &h));
  const double aspect = (double)w / h;

  size_t columns = 1;
  size_t rows    = 1;
  while (columns * rows < num_textures) {
    if (columns == rows)
      ++columns;
    else
      ++rows;
  }
  w = DISPLAY_WIDTH / columns;
  h = (int)(aspect * w);
  if (rows * h > DISPLAY_HEIGHT) {
    h = DISPLAY_HEIGHT / rows;
    w = (int)(h / aspect);
  }

  SDL_Rect tgt = (SDL_Rect){
      .x = 0,
      .y = 0,
      .w = w,
      .h = h,
  };

  for (size_t i = 0; i < num_textures; ++i) {
    tgt.x = (i % columns) * tgt.w;
    tgt.y = (i / columns) * tgt.h;

    const SDL_Rect projected = project_ortho_to_rect(tgt);
    ERRCHK(!SDL_RenderCopy(renderer, textures[i], NULL, &projected));
  }

  // Present
  SDL_RenderPresent(renderer);

  // Evaluate input and return -1 if exit requested, 0 otherwise
  const float translate_rate = frametime_ms * 1.0f / camera.scale;
  const float scale_rate     = 1.01f + frametime_ms * 0.005f;
  if (input_keydown(ZOOM_IN))
    camera.scale *= scale_rate;
  if (input_keydown(ZOOM_OUT))
    camera.scale /= scale_rate;
  if (input_keydown(MV_LEFT))
    camera.pos.x -= translate_rate;
  if (input_keydown(MV_RIGHT))
    camera.pos.x += translate_rate;
  if (input_keydown(MV_UP))
    camera.pos.y += translate_rate;
  if (input_keydown(MV_DOWN))
    camera.pos.y -= translate_rate;

  return eval_input();
}

/*
 * =============================================================================
 * Texture
 * =============================================================================
 */
size_t
texture_create(const size_t w, const size_t h)
{
  ERRCHK(num_textures < MAX_NUM_TEXTURES);

  textures[num_textures] = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
                                             SDL_TEXTUREACCESS_STREAMING, w, h);
  ERRCHK_SDL(textures[num_textures]);
  return num_textures++;
}

void
texture_update(const real* data, const size_t handle)
{
  int w, h;
  ERRCHK(!SDL_QueryTexture(textures[handle], NULL, NULL, &w, &h));

  int32_t* pixels;
  int pitch;
  ERRCHK_SDL(!SDL_LockTexture(textures[handle], NULL, (void**)&pixels, &pitch));
  // for (int i = 0; i < w * h; ++i)
  //   pixels[i] = data[i] >= (real)0.5 ? 0xffffffff : 0x0;

  const size_t count = w * h;
  const real diff    = arange(data, count);
  const real x0      = reduce(data, count, value, min);

  for (int i = 0; i < w * h; ++i) {
    // printf("data %g -> %f\n", data[i], 255 * data[i]);
    uint8_t color[4] = {0xff, 0x0, 0x0, (uint8_t)(255 * (data[i] - x0) / diff)};
    pixels[i]        = *(uint32_t*)color;
  }

  SDL_UnlockTexture(textures[handle]);
}

#else
static void
show_warning(void)
{
  WARNING("Visualization not enabled. Must set CMake option VISUALIZATION=ON.");
}

void
display_init(void)
{
  show_warning();
}

int
display_refresh(void)
{
  show_warning();
  return -1;
}

void
display_quit(void)
{
  show_warning();
}
size_t
texture_create(const size_t w, const size_t h)
{
  (void)w;
  (void)h;
  show_warning();
  return -1;
}

/** Updates the contents of the texture with data. The array
`data` must be large enough to fit into the texture. */
void
texture_update(const real* data, const size_t texture)
{
  (void)data;
  (void)texture;
  show_warning();
}
#endif
