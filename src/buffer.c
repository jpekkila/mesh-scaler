/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "buffer.h"

#include <math.h>
#include <stdio.h>

#include "errchk.h"
#include "utils.h"

static long double
abs_error(const real model, const real candidate)
{
  return fabsl((long double)model - (long double)candidate);
}

static long double
ulp_error(const real model, const real candidate)
{
  const int base = 2;

  // See IEEE 754
  // https://en.wikipedia.org/wiki/Quadruple-precision_floating-point_format
  // https://en.wikipedia.org/wiki/Double-precision_floating-point_format
  // https://en.wikipedia.org/wiki/Single-precision_floating-point_format
#if SIZEOF_REAL == 16
  const int p = 113; // Bits of precision in the significant (-1 bit explicit)
#elif SIZEOF_REAL == 8
  const int p = 24;
#elif SIZEOF_REAL == 4
  const int p = 53;
#endif

  const long double e   = floorl(logl(fabsl((long double)model)) / logl(2));
  const long double ulp = powl(base, e - (p - 1));

  return abs_error(model, candidate) / ulp;
}

static void
compare_arrays(const real* model, const real* candidate, const size_t count)
{
  long double largest_abs = 0;
  long double largest_ulp = 0;

  for (size_t i = 0; i < count; ++i) {

    const long double abs_err = abs_error(model[i], candidate[i]);
    if (abs_err > largest_abs)
      largest_abs = abs_err;

    const long double ulp_err = ulp_error(model[i], candidate[i]);
    if (ulp_err > largest_ulp)
      largest_ulp = ulp_err;

#if VERBOSE
    printf("%-18s%f\n", "Model:", model[i]);
    printf("%-18s%f\n", "Candidate:", candidate[i]);
    printf("%-18s%Lg\n", "Abs error:", abs_err);
    printf("%-18s%Lg\n", "ULP error:", ulp_err);
    printf("%-18s10^%g\n\n", "Error magnitude:", floor(log10(abs_err)));
#endif
  }

  printf("Compared arrays %p and %p of count %lu\n", (void*)model,
         (void*)candidate, count);
  printf("\tLargest abs error: %Lg\n", largest_abs);
  printf("\tLargest ulp error: %Lg\n", largest_ulp);
}

Buffer
buffer_create(const size_t count)
{
  Buffer buffer = (Buffer){
      .count = count,
      .data  = calloc(count, sizeof(buffer.data[0])),
  };
  ERRCHK(buffer.data);

  return buffer;
}

Buffer
buffer_clone(const Buffer in)
{
  Buffer out = buffer_create(in.count);
  for (size_t i = 0; i < in.count; ++i)
    out.data[i] = in.data[i];

  return out;
}

void
buffer_destroy(Buffer* buffer)
{
  buffer->count = 0;

  free(buffer->data);
  buffer->data = NULL;
}

void
buffer_print(const Buffer buffer)
{
  for (size_t i = 0; i < buffer.count; ++i)
    printf("%Lg\n", (long double)buffer.data[i]);
}

void
buffer_compare(const Buffer model, const Buffer candidate)
{
  ERRCHK(model.count == candidate.count);
  compare_arrays(model.data, candidate.data, model.count);
}

void
buffer_set(const InitcondType initcond, Buffer* buf)
{
  switch (initcond) {
  case ZERO: {
    for (size_t i = 0; i < buf->count; ++i)
      buf->data[i] = 0.0;
  } break;
  case LINEAR: {
    for (size_t i = 0; i < buf->count; ++i)
      buf->data[i] = (real)i;
  } break;
  case RANDOM: {
    for (size_t i = 0; i < buf->count; ++i)
      buf->data[i] = (real)randr();
  } break;
  default:
    ERROR("Invalid initcond passed to buffer_set");
  }
}

Buffer
buffer_create_from_file(const char* path)
{
  FILE* fp = fopen(path, "r");
  ERRCHK(fp);

  // Determine size
  ERRCHK(!fseek(fp, 0, SEEK_END));
  const long bytes = ftell(fp);
  ERRCHK(bytes != -1L);

  const size_t count = bytes / sizeof(real);
#if VERBOSE
  printf("Created a buffer from file, count %lu\n", count);
#endif

  Buffer buffer = buffer_create(count);

  rewind(fp);
  const size_t retval = fread(buffer.data, sizeof(buffer.data[0]), buffer.count,
                              fp);
  ERRCHK(retval == buffer.count);

  fclose(fp);
  return buffer;
}

void
buffer_to_file(const Buffer buffer, const char* path)
{
  FILE* fp = fopen(path, "w");
  ERRCHK(fp);

  const size_t retval = fwrite(buffer.data, sizeof(buffer.data[0]),
                               buffer.count, fp);
  ERRCHK(retval == buffer.count);

  fclose(fp);
}

void
abs_error_to_file(const Buffer model, const Buffer candidate, const char* path)
{
  ERRCHK(model.count == candidate.count);

  FILE* fp = fopen(path, "w");
  ERRCHK(fp);

  for (size_t i = 0; i < model.count; ++i)
    fprintf(fp, "%Lf\n", abs_error(model.data[i], candidate.data[i]));

  fclose(fp);
}

void
ulp_error_to_file(const Buffer model, const Buffer candidate, const char* path)
{
  ERRCHK(model.count == candidate.count);

  FILE* fp = fopen(path, "w");
  ERRCHK(fp);

  for (size_t i = 0; i < model.count; ++i)
    fprintf(fp, "%Lf\n", ulp_error(model.data[i], candidate.data[i]));

  fclose(fp);
}
