/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#define SIZEOF_REAL (8) // Bytes

#if SIZEOF_REAL == 16
typedef long double real;
#elif SIZEOF_REAL == 8
typedef double real;
#elif SIZEOF_REAL == 4
typedef float real;
#endif

_Static_assert(sizeof(real) == SIZEOF_REAL,
               "sizeof(real) did not match with SIZEOF_REAL");
