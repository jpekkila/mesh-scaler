/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "buffer.h"

typedef struct {
  size_t x, y, z;
} Volume;

typedef struct {
  int x, y, z;
} int3;

typedef struct {
  Volume volume;
  Buffer buffer;
} Block;

typedef struct {
  int3 offset;
  Volume volume;
} Window;

Block block_create(const Volume volume);

/** Reads a file into a data block.
 * `path`: path to the file
 * `volume`: volume of the mesh stored in the file
 * `window`: window within the volume that is read into the block
 *
 * The returned block will be of size window.volume.
 * The returned block must be freed with `block_destroy` after use to avoid a
 *  memory leak.
 */
Block block_create_from_file(const char* path, const Volume volume,
                             const Window window);

void block_destroy(Block* block);

void block_print(const Block block);

void block_interpolate(const Block in, Block* out);

/** Writes a data block to a file. Requires that the file already exists.
 *  `in`: input data block
 *  `in_window`: window in the input data block to be written to the file
 *  `path`: path to the output file
 *  `out_window`: offset in the output file where the contents of the in_window
 *                are written and the total volume of the output file.
 *
 * Notes: The file is not created, cleared, nor resized with this function.
 * If writing multiple smaller input blocks to a larger output file, consider
 * clearing the file first with `file_clear()`.
 */
void block_to_file(const Block in, const Window in_window, const char* path,
                   const Window out_window);

void buffer_compare_window(const Buffer model, const Buffer candidate,
                           const Volume volume, const Window window);

void block_set(const int3 halo, Block* block);
