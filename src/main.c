/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>

#include "analysis.h"
#include "block.h"
#include "buffer.h"
#include "errchk.h"
#include "test.h"
#include "utils.h"

#if 1
/*
cmake .. && make -j && ./mesh-scaler --input=var.dat\
--volume=294,294,294 --num-fields=8 --visualize=0 --output-volume=256,256,256\
--halo-offset=3,3,3
*/

int
main(int argc, char* argv[])
{
  char input[4096]     = "";
  Volume volume        = (Volume){0, 0, 0};
  Volume output_volume = (Volume){0, 0, 0};
  size_t num_fields    = 0;
  int visualize        = 0;
  int3 halo_offset     = (int3){0, 0, 0};

  for (int i = 1; i < argc; ++i) {
    sscanf(argv[i], "--input=%4095s", input);
    sscanf(argv[i], "--volume=%lu,%lu,%lu", &volume.x, &volume.y, &volume.z);
    sscanf(argv[i], "--num-fields=%lu", &num_fields);
    sscanf(argv[i], "--output-volume=%lu,%lu,%lu", &output_volume.x,
           &output_volume.y, &output_volume.z);
    sscanf(argv[i], "--visualize=%d", &visualize);
    sscanf(argv[i], "--halo-offset=%d,%d,%d", &halo_offset.x, &halo_offset.y,
           &halo_offset.z);
  }

  printf("Input path: %s\n", input);
  printf("Input volume: (%lu, %lu, %lu)\n", volume.x, volume.y, volume.z);
  printf("Output volume: (%lu, %lu, %lu)\n", output_volume.x, output_volume.y,
         output_volume.z);
  printf("Num fields: %lu\n", num_fields);
  printf("Visualize: %d\n", visualize);
  printf("Halo offset: (%d, %d, %d)\n", halo_offset.x, halo_offset.y,
         halo_offset.z);

  ERRCHK(volume.x > 0);
  ERRCHK(volume.y > 0);
  ERRCHK(volume.z > 0);
  ERRCHK(output_volume.x > 0);
  ERRCHK(output_volume.y > 0);
  ERRCHK(output_volume.z > 0);
  ERRCHK(num_fields > 0);
  ERRCHK(halo_offset.x >= 0);
  ERRCHK(halo_offset.y >= 0);
  ERRCHK(halo_offset.z >= 0);

  if (visualize)
    display_varfile(input, volume, num_fields);

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))
  const char* field_names[] = {
      "VTXBUF_AX.out",    "VTXBUF_AY.out",      "VTXBUF_AZ.out",
      "VTXBUF_LNRHO.out", "VTXBUF_ENTROPY.out", "VTXBUF_UUX.out",
      "VTXBUF_UUY.out",   "VTXBUF_UUZ.out",

  };
  WARNCHK(ARRAY_SIZE(field_names) == num_fields);

  for (size_t i = 0; i < num_fields; ++i) {

    const Volume file_volume = (Volume){
        volume.x,
        volume.y,
        num_fields * volume.z,
    };
    const int3 in_offset = (int3){
        halo_offset.x,
        halo_offset.y,
        halo_offset.z + i * volume.z,
    };
    const Volume in_volume = (Volume){
        volume.x - 2 * halo_offset.x,
        volume.y - 2 * halo_offset.y,
        volume.z - 2 * halo_offset.z,
    };
    const Window window = (Window){.offset = in_offset, .volume = in_volume};

    Block in = block_create_from_file(input, file_volume, window);

    Block out = block_create(output_volume);
    block_interpolate(in, &out);

    buffer_print_analysis("Buffer", out.buffer);

    const Window out_window = (Window){
        .offset = (int3){0, 0, 0},
        .volume = output_volume,
    };
    file_clear(field_names[i]);
    block_to_file(out, out_window, field_names[i], out_window);

    /*
    Block test = block_create_from_file(field_names[i], out_window.volume,
                                        out_window);
    //display_varfile(field_names[i], out_window.volume, 1);
    // buffer_compare(out.buffer, test.buffer);
    block_destroy(&test);
    */

    block_destroy(&out);
    block_destroy(&in);
  }
  /*
  Block test = block_create(volume);
  block_set((int3){3, 3, 3}, &test);

  const Window window = (Window){
      .offset = (int3){0, 0, 0},
      .volume = volume,
  };
  block_to_file(test, window, "test.out", window);
  block_destroy(&test);
  */

  // display_varfile(input, volume, num_fields);
  // display_fields(field_names, num_fields, output_volume);
  return EXIT_SUCCESS;
}

#else
int
main(int argc, char* argv[])
{
  char input[4096]     = "";
  Volume volume        = (Volume){0, 0, 0};
  Volume output_volume = (Volume){0, 0, 0};
  size_t num_fields    = 0;
  int visualize        = 0;

  printf("Usage: ./mesh-scaler --input=<path> --volume=<mx>,<my>,<mz> "
         "--output-volume=<mx>,<my>,<mz> --num-fields=<count> "
         "--visualize=<1|0>\n");

  for (int i = 0; i < argc; ++i) {
    sscanf(argv[i], "--input=%4095s", input);
    sscanf(argv[i], "--volume=%lu,%lu,%lu", &volume.x, &volume.y, &volume.z);
    sscanf(argv[i], "--num-fields=%lu", &num_fields);
    sscanf(argv[i], "--output-volume=%lu,%lu,%lu", &output_volume.x,
           &output_volume.y, &output_volume.z);
    sscanf(argv[i], "--visualize=%d", &visualize);
  }
  printf("Input path: %s\n", input);
  printf("Input volume: (%lu, %lu, %lu)\n", volume.x, volume.y, volume.z);
  printf("Output volume: (%lu, %lu, %lu)\n", output_volume.x, output_volume.y,
         output_volume.z);
  printf("Num fields: %lu\n", num_fields);
  printf("Visualize: %d\n", visualize);
  ERRCHK(volume.x > 0);
  ERRCHK(volume.y > 0);
  ERRCHK(volume.z > 0);
  ERRCHK(output_volume.x > 0);
  ERRCHK(output_volume.y > 0);
  ERRCHK(output_volume.z > 0);
  ERRCHK(num_fields > 0);

  if (visualize)
    display_varfile(input, volume, num_fields);

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))
  const char* field_names[] = {
      "VTXBUF_UUX.out",     "VTXBUF_UUY.out",   "VTXBUF_UUZ.out",
      "VTXBUF_AX.out",      "VTXBUF_AY.out",    "VTXBUF_AZ.out",
      "VTXBUF_ENTROPY.out", "VTXBUF_LNRHO.out",

  };
  ERRCHK(ARRAY_SIZE(field_names) == num_fields);

  for (size_t i = 0; i < ARRAY_SIZE(field_names); ++i) {
    const Window window = (Window){
        .offset = (int3){0, 0, i * volume.z},
        .volume = volume,
    };
    Block in  = block_create_from_file(input,
                                       (Volume){volume.x, volume.y,
                                               num_fields * volume.z},
                                       window);
    Block out = block_create(output_volume);
    block_interpolate(in, &out);

    const Window out_window = (Window){
        .offset = (int3){0, 0, 0},
        .volume = output_volume,
    };
    file_clear(field_names[i]);
    block_to_file(out, out_window, field_names[i], out_window);

    /*
    Block test = block_create_from_file(field_names[i], out_window.volume,
                                        out_window);
    buffer_compare(out.buffer, test.buffer);
    block_destroy(&test);
    */

    // Strip halo
    // 1) load to volume nn, offset halo
    // 2) store to volume nn, offset 0
    const size_t halo      = 3;
    const int3 halo_offset = (int3){halo, halo, halo};
    const int3 zero_offset = (int3){0, 0, 0};
    const Volume volume_nn = (Volume){
        output_volume.x - 2 * halo,
        output_volume.y - 2 * halo,
        output_volume.z - 2 * halo,
    };
    const Window win_in  = (Window){.offset = halo_offset, .volume = volume_nn};
    const Window win_out = (Window){.offset = zero_offset, .volume = volume_nn};
    Block stripped       = block_create_from_file(field_names[i], output_volume,
                                                  win_in);
    block_to_file(stripped, win_out, field_names[i], win_out);
    block_destroy(&stripped);

    block_destroy(&out);
    block_destroy(&in);
  }

  return EXIT_SUCCESS;
}
#endif
