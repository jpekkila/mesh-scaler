/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <stdbool.h>
#include <stdlib.h>

#include "datatypes.h"

void display_init(void);

void display_quit(void);

/** Returns -1 if exit was requested, 0 otherwise. */
int display_refresh(void);

/** Creates a texture and returns a handle to it. */
size_t texture_create(const size_t w, const size_t h);

/** Updates the contents of the texture with data. The array
`data` must be large enough to fit into the texture. */
void texture_update(const real* data, const size_t texture);

typedef enum {
  ZOOM_IN,
  ZOOM_OUT,
  MV_UP,
  MV_DOWN,
  MV_LEFT,
  MV_RIGHT,
  NUM_KEYS,
} Key;

/** Returns true if the associated key is down, false otherwise */
bool input_keydown(const Key code);
