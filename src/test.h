/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

void buffer_test_rw(void);

void block_test_write_simple(void);

void block_test_write_advanced(void);

void block_test_read_simple(void);

void block_test_read_advanced(void);

void block_test_rw(void);

void interpolation_test_simple(void);

void interpolation_test_advanced(void);

void display_test_simple(void);

#include "block.h"

void display_test_varfile(const char* path, const Volume volume);

void tests_run(void);

void display_varfile(const char* path, const Volume volume,
                     const size_t num_fields);

void display_fields(const char* fields[], const size_t num_fields,
                    const Volume volume);
