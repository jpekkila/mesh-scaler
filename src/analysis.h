/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <stdbool.h>

#include "buffer.h"
#include "datatypes.h"

typedef real (*fn_unary)(const real value);
typedef real (*fn_binary)(const real a, const real b);
typedef bool (*fn_bool)(const real x);

real value(const real a);
real min(const real a, const real b);
real max(const real a, const real b);

/**
 * Performs a reduction on an array. The function `unary_op` is applied to each
 * element independently, and `binary_op` is used to reduce the processed
 * elements.
 *
 * If `unary_op` is NULL, the unary operation returns the element itself.
 */
real reduce(const real* data, const size_t count, const fn_unary unary_op,
            const fn_binary binary_op);

/** Map `x = fn(x)` */
void map(const size_t count, const fn_unary fn, real* x);

/** Map `x = fn(x, y)` */
void map2(const size_t count, const fn_binary fn, real* x, const real* y);

/** Filter elements satisfying `fn` and return the count of filtered elems */
size_t filter(const size_t count, const fn_bool fn, real* x);

/** Returns the diff between the largest and smallest values in the array */
real arange(const real* data, const size_t count);

real expected_value(const Buffer buffer);

real variance(const Buffer buffer);

real standard_deviation(const Buffer buffer);

/** Returns the diff between the largest and smallest values in the buffer */
real range(const Buffer buffer);

/** Returns the mean squared quantization error (msqe) */
real distortion(const Buffer model, const Buffer candidate);

/** Prints various metrics of the buffer values */
void buffer_print_analysis(const char* str, const Buffer buffer);
