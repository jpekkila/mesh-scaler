/*
    Copyright (C) 2022, Johannes Pekkila.

    This file is part of MESH-SCALER.

    MESH-SCALER is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MESH-SCALER is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MESH-SCALER.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

typedef struct timespec Timer;

/** Returns the result of clock_getttime (0 on success, -1 on failure) */
int timer_reset(Timer* t);

/** Returns the time elapsed since timer_reset was called in nanoseconds */
long timer_diff_nsec(const Timer start);

/** Prints the time elapsed since timer_reset was called in milliseconds */
void timer_diff_print(const Timer t);
